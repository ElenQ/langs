import csv
import json
import sqlite3
import os


source_folder = 'data'

def fileload( filename ):
    data = {}
    with open(filename) as f:
        reader = csv.DictReader(f)
        for i in reader:
            data = {**data, **{ i['id']: i['value']}}
    return data

def getNativeName( x ):
    """
    Tries to find the language, if it's not, it will be discarded later
    """
    try:
        return (x[0], x[1][x[0]])
    except:
        return None

def mergecategory( conts, catname ):
    return tuple(map(lambda x: dict(x, CATEGORIA=catname), conts))

langnames = os.listdir(source_folder)

# This lines remove the origin distinction, comment it to get all
langnames = tuple(filter(lambda x: '_' not in x, langnames))

# Read the all the CSVs and make a tuple of tuples out of it
fullpaths = tuple(map(lambda x: os.path.join( source_folder, x, 'language.csv'), langnames))
read      = map(lambda x, y: (y, fileload(x)), fullpaths, langnames)
read      = map(getNativeName, read)

# Discard missing
read      = filter(lambda x: x is not None, read)

# Uppercase first char to make them consistent
read      = tuple(map(lambda x: (x[0],x[1][0].upper()+x[1][1:]), read))

# Dump a languages.json file
with open("languages.json", "w") as f:
    f.write(json.dumps(dict(read), indent=4))

# Dump a languages.csv file
with open("languages.csv", "w") as f:
    writer = csv.writer(f)
    writer.writerow(('id', 'value'))
    for i in read:
        writer.writerow(i)

# Dump a languages.sqlite.sql file
with open("languages.sqlite.sql", "w") as f:
    conn = sqlite3.connect(":memory:")
    c = conn.cursor()
    c.execute('CREATE TABLE languages (id TEXT PRIMARY KEY, language TEXT)')
    c.executemany('INSERT INTO languages VALUES (?,?)', list(read))
    for i in conn.iterdump():
        f.write(i+'\n')
    conn.close()
