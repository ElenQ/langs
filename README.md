# Language list

This repo is a language list with the languages written in their own language.
Data have been obtained from:

https://github.com/umpirsky/language-list.git

**NOTE**: Languages variations are not considered. For example, there's only
one Spanish (Español), there's not Spanish from Spain, Spanish from Mexico,
Spanish from *Whatever place* distinction.

If you want to extract the data again, download that repo and execute the
`tools/langs.py` script in the repo's root folder. The script has more info
about how it works.
